# Dummyjson API Test

This project aims to automate DummyJson API testing using Java as the main language.

## Requirements

- Git
- Intelij
- Java JDK 17
- Maven

## Installation

Clone the project in your preferred directory.
Open the project in the Intelij IDE.
Run the `mvn clean install package` command in the terminal to install the dependencies.
To create the test report in a browser, run the following commands in the terminal: `mvn site` and `mvn surefire-report:report`

## Test Scenarios

- Login successfully: Sending a request with valid credentials I expect to receive a 201 OK response and create an authentication token.
- Login with invalid credentials: Sending a request with invalid credentials i expect to receive a 400 response.
- Search products with authentication: Using the user authentication token i want to view the available products.
- Search products with invalid token: Using an invalid or expired token, when requesting to view products, i may receive a 401 unauthorized response
- Search products without a token: Send a request without a token i want to receive a 403 Forbidden response 
- Adding products: Sending a request with valid values i expect to receive a 201 OK response and and check the added product.
- Find a product: Send a request using a valid id i expect to receive a 200 OK response with the product informations.
- Find a product with invalid id: Send a request usins a invalid id, i want to receive a 404 response with a error message.
- Viewing all products: Send a request using `/products` in the url and expect to receive a 200 response and view all added products.