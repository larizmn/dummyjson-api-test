package com.larizmn.tests;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.net.URI;
import java.net.http.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.text.StringEscapeUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DummyJsonAPITest {

  private static final String LOGIN_URL = "https://dummyjson.com/auth/login";
  private static final String ADD_PRODUCT_URL = "https://dummyjson.com/products/add";

  private static final String AUTH_PRODUCTS_URL = "https://dummyjson.com/auth/products";

  private static final String TEST_URL = "https://dummyjson.com/test";
  private static final String USERS_URL = "https://dummyjson.com/users";
  private static final String PRODUCTS_URL = "https://dummyjson.com/products";
  private static final String TESTING_URL = "https://dummyjson.com/testing";

  private HttpClient httpClient;

  @BeforeEach
  void setUp() {
    this.httpClient = HttpClient.newHttpClient();
  }

  private HttpResponse<String> getRequest(String url) throws IOException, InterruptedException {
    var request = HttpRequest.newBuilder()
            .header("Content-Type", "application/json")
            .uri(URI.create(url))
            .build();
    return this.httpClient.send(request, HttpResponse.BodyHandlers.ofString());
  }

  private HttpResponse<String> getRequest(String url, String... headers) throws IOException, InterruptedException {
    var request = HttpRequest.newBuilder()
            .header("Content-Type", "application/json")
            .headers(headers)
            .uri(URI.create(url))
            .build();
    return this.httpClient.send(request, HttpResponse.BodyHandlers.ofString());
  }

  private HttpResponse<String> postRequest(String url, String requestBody) throws IOException, InterruptedException {
    var request = HttpRequest.newBuilder()
            .header("Content-Type", "application/json")
            .uri(URI.create(url))
            .POST(HttpRequest.BodyPublishers.ofString(requestBody, StandardCharsets.UTF_8))
            .build();
    return this.httpClient.send(request, HttpResponse.BodyHandlers.ofString());
  }

  private void assertSuccessfulRequest(HttpResponse<String> response) {
    assertHttpResponseStatusCodeInSuccessRange(response.statusCode());
    assertThat(response.headers().firstValue("content-type")).isPresent().get().isEqualTo("application/json; charset=utf-8");
  }

  private String getResourceAsString(String fileName) throws IOException {
    String resourceAsString = new String(getClass().getClassLoader().getResourceAsStream(fileName).readAllBytes());
    return StringEscapeUtils.unescapeHtml4(resourceAsString);
  }

  @Test
  public void testProductNotFound() throws Exception {
    HttpResponse<String> response = getRequest(PRODUCTS_URL + "/0");
    assertThat(response.statusCode()).isEqualTo(404);
    String expectedValue = getResourceAsString("testProductNotFoundExpetedValue.json");
    String responseValue = StringEscapeUtils.unescapeHtml4(response.body());
    assertThat(responseValue).isEqualTo(expectedValue);
  }

  @Test
  public void testExistingProduct() throws Exception {
    HttpResponse<String> response = getRequest(PRODUCTS_URL + "/1");
    assertSuccessfulRequest(response);
    String expectedValue = getResourceAsString("testExistingProductExpectedValue.json");
    String responseValue = StringEscapeUtils.unescapeHtml4(response.body());
    assertThat(responseValue).isEqualTo(expectedValue);
  }

  @Test
  public void testGetAllProducts() throws Exception {
    HttpResponse<String> response = getRequest(PRODUCTS_URL);
    assertSuccessfulRequest(response);
    String expectedValue = getResourceAsString("testGetAllProductsExpectedValue.json");
    String responseValue = StringEscapeUtils.unescapeHtml4(response.body());
    assertThat(responseValue).isEqualTo(expectedValue);
  }

  @Test
  public void testUsers() throws Exception {
    HttpResponse<String> response = getRequest(USERS_URL);
    assertSuccessfulRequest(response);
    String expectedValue = getResourceAsString("testUsersExpectedValue.json");
    String responseValue = StringEscapeUtils.unescapeHtml4(response.body());
    assertThat(responseValue).isEqualTo(expectedValue);
  }

  @Test
  public void testGetTestURL() throws Exception {
    HttpResponse<String> response = getRequest(TEST_URL);
    assertSuccessfulRequest(response);
    String expectedValue = getResourceAsString("testURLExpectedValue.json");
    String responseValue = StringEscapeUtils.unescapeHtml4(response.body());
    assertThat(responseValue).isEqualTo(expectedValue);
  }

  @Test
  public void testInvalidURL() throws Exception {
    HttpResponse<String> response = getRequest(TESTING_URL);
    assertThat(response.statusCode()).isEqualTo(404);
  }

  @Test
  public void testLogin() throws Exception {
    String requestBody = getResourceAsString("userLoginData.json");
    HttpResponse<String> response = postRequest(LOGIN_URL, requestBody);
    assertSuccessfulRequest(response);
    ObjectMapper objectMapper = new ObjectMapper();
    String unescapedResponse = StringEscapeUtils.unescapeHtml4(response.body());
    ObjectNode responseBody = (ObjectNode) objectMapper.readTree(unescapedResponse);
    responseBody.remove(Set.of("token", "image", "email"));
    String expectedValue = getResourceAsString("testLoginExpectedValue.json");
    assertThat(responseBody.toString()).isEqualTo(expectedValue);
  }

  @Test
  public void testInvalidUserLogin() throws Exception {
    String requestBody = getResourceAsString("invalidUserLoginData.json");
    HttpResponse<String> response = postRequest(LOGIN_URL, requestBody);
    assertThat(response.statusCode()).isEqualTo(400);
    String expectedValue = getResourceAsString("testInvalidUserLoginExpectedValue.json");
    String responseValue = StringEscapeUtils.unescapeHtml4(response.body());
    assertThat(responseValue).isEqualTo(expectedValue);
  }

  @Test
  public void testAuthenticatedProducts() throws Exception {
    String requestBody = getResourceAsString("userLoginData.json");
    HttpResponse<String> response = postRequest(LOGIN_URL, requestBody);
    assertSuccessfulRequest(response);
    ObjectMapper mapper = new ObjectMapper();
    Map<String,Object> map = mapper.readValue(response.body(), Map.class);
    response = getRequest(AUTH_PRODUCTS_URL, "Authorization", map.get("token").toString());
    assertSuccessfulRequest(response);
    String expectedValue = getResourceAsString("testAuthenticatedProductsExpectedValue.json");
    String responseValue = StringEscapeUtils.unescapeHtml4(response.body());
    assertThat(responseValue).isEqualTo(expectedValue);
  }

  @Test
  public void testAuthenticatedProductsAccessForbidden() throws Exception {
    HttpResponse<String> response = getRequest(AUTH_PRODUCTS_URL);
    assertThat(response.statusCode()).isEqualTo(403);
    String expectedValue = getResourceAsString("testProductsAccessForbiddenExpectedValue.json");
    String responseValue = StringEscapeUtils.unescapeHtml4(response.body());
    assertThat(responseValue).isEqualTo(expectedValue);
  }

  @Test
  public void testAuthenticatedProductsUnauthorized() throws Exception {
    HttpResponse<String> response = getRequest(AUTH_PRODUCTS_URL, "Authorization", "foo");
    assertThat(response.statusCode()).isEqualTo(401);
    String expectedValue = getResourceAsString("testProductsUnauthorizedExpectedValue.json");
    String responseValue = StringEscapeUtils.unescapeHtml4(response.body());
    assertThat(responseValue).isEqualTo(expectedValue);
  }

  @Test
  public void testAddProduct() throws Exception {
    String requestBody = new String(getClass().getClassLoader().getResourceAsStream("addProductData.json").readAllBytes());
    HttpResponse<String> response = postRequest(ADD_PRODUCT_URL, requestBody);
    assertHttpResponseStatusCodeInSuccessRange(response.statusCode());
    String expectedValue = getResourceAsString("testAddProductExpectedValue.json");
    String responseValue = StringEscapeUtils.unescapeHtml4(response.body());
    assertThat(responseValue).isEqualTo(expectedValue);
  }

  private void assertHttpResponseStatusCodeInSuccessRange(int responseStatus) {
    assertThat(responseStatus).isBetween(200, 299);
  }
}